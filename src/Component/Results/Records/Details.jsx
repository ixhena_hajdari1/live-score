import React, { Component } from 'react';
import Outcomes from './Outcomes';

import Out from './Out';
import OutcomesDetails from './OutcomesDetails';

export default Details;
function Details(props) {
  return(           
     
      <div className="d3">

  
      {props.children.map((outcome, i) => <OutcomesDetails outcome={outcome} key={i} />)}
     
      {console.log(props.children)}
      </div>
  )
} 