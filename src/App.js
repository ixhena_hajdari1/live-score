import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from './Component/Header/Header.jsx';
import Result from './Component/Results/Result.jsx';

// import Out from './Component/'
import HM from './Component/Results/Details/HM.jsx';

import AW from './Component/Results/Details/AW.jsx';
import Out from './Component/Results/Records/Out.jsx';
import './App.css';
import {
  BrowserRouter as Router,
} from 'react-router-dom'
import Redc from './Component/Results/Details/Redc.jsx';
import Details from './Component/Results/Records/Details.jsx';


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // buildSportSelected: {},
      sc_ep1: {},
      sc_el1: {},
      liveStore: [],
      temp: {},
      tempStoreLive:{
        },
      // data: [
      //   this.state.buildSportSelected()
      // ],
      // liveStore : [ 
      //   this.sportSelectedAllIds,
      //   this.matchById,
      // ],
      away: [],
      market: [
        {
          id: '1', name: 'FT', outcomes: [
            { name: "1" },
            { name: "X" },
            { name: "2" }
          ]
        },
        {
          id: '2', name: 'NG', outcomes: [
            { name: "1" },
            { name: "X" },
            { name: "2" }
          ]
        },
        {
          id: '3', name: 'TG', outcomes: [
            { name: "G" },
            { name: "O" },
            { name: "U" }
          ]
        },
        {
          id: '4', name: 'GG/NG', outcomes: [
            { name: "G" },
            { name: "N" },

          ]
        }
      ],
    }
  }

  componentDidMount() {
    const url = "http://m.bit-test.com/rdst"
    fetch(url, {
      method: 'POST',
      body: JSON.stringify([["g", ["sc_sl", "sc_el1", "sc_gml1"]], ["ga", ["cml1", "ctml1_en"]]])

    })

      .then((response) => response.text()) // or response.json()
      .then(response => {
        var temp = JSON.parse(response);
        // const matchAllIds = buildSportSelected(1, temp[0]['sc_el1']).matchAllIds;
        // const sportSelectedAllIds = buildSportSelected(1,temp[0]['sc_el1']).sportSelectedAllIds;

        // console.log(matchAllIds)
        // this.setState({
        // sc_sl: temp[0]['sc_sl'],
        // sc_el1: temp[1]['sc_el1'],
        // cml1: temp[2]['cml1'],
        // sc_gml1: temp[3]['sc_gml1'],

        // });
        // const sportSelectedCategAllIds = buildSportSelected(1, temp[0]['sc_el1']).sportSelectedCategAllIds;
        const matchId = buildSportSelected(1, temp[1]["sc_el1"], 1).matchId;
     
        const clonesStore = buildSportSelected(1, temp[1]["sc_el1"], 1).clonesStore;
        const matchStore = buildSportSelected(1, temp[1]["sc_el1"], 1).matchStore;
        const matchById = buildSportSelected(1, temp[1]["sc_el1"], 1).matchById;
        const tourById = buildSportSelected(1, temp[1]["sc_el1"], 1).tourById;
        const tourId = buildSportSelected(1, temp[1]["sc_el1"], 1).tourId;
        const groupId = buildSportSelected(1, temp[1]["sc_el1"], 1).groupId;
        const matchAllIds = buildSportSelected(1, temp[1]["sc_el1"], 1).matchAllIds;
        const configCateg = buildSportSelected(1, temp[1]["sc_el1"], 1).configCateg;
        const configTour = buildSportSelected(1, temp[1]["sc_el1"], 1).configTour;
        const configGroup = buildSportSelected(1, temp[1]["sc_el1"], 1).configGroup;
        const configLive = buildSportSelected(1, temp[1]["sc_el1"], 1).configLive;
        const marketAllIds = buildMarkets(1, temp[3]["cml1"]).marketAllIds;
        const marketById = buildMarkets(1, temp[3]["cml1"]).marketById;
        const outcomeAllIds = buildMarkets(1, temp[3]["cml1"]).outcomeAllIds;
        const outcomeById = buildMarkets(1, temp[3]["cml1"]).outcomeById;
        const marketStore = buildMarkets(1, temp[3]["cml1"]).marketStore;
        const outcomeId = buildMarkets(1, temp[3]["cml1"]).outcomeId;
        const categById = buildSportSelected(1, temp[1]["sc_el1"], 1).categById;
        const categId = buildSportSelected(1, temp[1]["sc_el1"], 1).categId;
        const marketId = buildMarkets(1, temp[3]["cml1"]).marketId;
        const market = buildMarkets(1, temp[3]["cml1"]).market; 
         const liveStore = buildSportSelected(1, temp[1]["sc_el1"], 1,market).liveStore;
        console.log(liveStore)
        const marketGroups = buildMarketGroups(1, temp[2]['sc_gml1']).marketGroups;
        const marketTranslation = buildMarketsTranslation(temp[4]['ctml1_en'],marketStore).marketTranslation;
        console.log(marketTranslation)
        // const outcomeOddObj = buildMatchOdds( matchId,marketId,marketStore,matchStore).outcomeOddObj
        // const outcomeOddObj = buildMatchOdds( matchId,marketId,marketStore,matchStore).outcomeOddObj
        // const mbbSbv = getMbSbv(market,marketById,marketId).mbbSbv;
        // const marketStore = buildMarkets(1, temp[1]['cml1']).marketStore;
        // const outcomeId = buildMarkets(1, temp[1]['cml1']).outcomeId;
        const sportById = buildSportList(temp[0]['sc_sl']).sportById;
        console.log(sportById)
        const sportAllIds = buildSportList(temp[0]['sc_sl']).sportAllIds;
        console.log(sportAllIds)
        const tempStoreLive = getSportCollection(1, temp).tempStoreLive;
        console.log(tempStoreLive)
        // this.setState({ matchById: matchById })    
        var config;
        config = ['cs1_1']
        config = config.concat(configCateg);
        config = config.concat(configLive);
        config = config.concat(configTour);
        config = config.concat(configGroup);
        // console.log(config)
        fetch(url, {
          method: 'POST',
          body: JSON.stringify([["gf", matchAllIds, [1242, 1198, 1210, 1228, "score", "timer", "state"]], ["ga", config]])
        })
          .then((response) => response.text()) // or response.json()
          .then(response => {
            var temp = JSON.parse(response);
            const extraoddsFeed = getOdds(temp, 1).extraoddsFeed;
            const oddsFeed = getOdds(temp, 1).oddsFeed;
            console.log(oddsFeed)
            // const outputStore = buildMatchOdds(marketStore, outcomeAllIds, matchId, matchStore, oddsFeed).outputStore;
            this.setState({
            tempStoreLive:tempStoreLive
            });  
            const sportConfigStore = getOdds(temp, 1).sportConfigStore;
            console.log(sportConfigStore)
            const marketConfig = calculateMarketConfig(sportConfigStore, 1, categId, tourId, groupId, marketId, 1).marketConfig;
            // console.log(marketConfig)
            const inputStore = getOdds(temp).extraoddsFeed;
            // const mbbSbv = getMbSbv(marketById, marketId, outcomeAllIds, market).mbbSbv;
            // console.log(mbbSbv)

            // this.setState({
            //   inputStore: inputStore,

            // });
            // const marketConfig = calculateMarketConfig(1, sportConfigStore, categId, tourId, groupId, matchId, marketAllIds, -2).marketConfig;
            // const totalProb = calculateTotalProb(market, oddsFeed, marketById, marketId, outcomeById, matchById, matchId, 1).totalProb;
            // console.log(totalProb)
            // const outcomeOdd = calculateOdd(1, marketConfig, totalProb, 20, outcomeId).outcomeOdd;
            // console.log(outcomeOdd)
            // const outputStore = buildMatchOdds(sportConfigStore, marketStore, outcomeAllIds, matchId, marketId, matchStore, inputStore, inputStore).outputStore;
            //   const liveStore = list(marketId,matchId, matchById, oddsFeed,outputStore).liveStore;
            //  console.log(liveStore)
            // const odd = list(marketId,matchId, matchById, oddsFeed,outputStore).odd;

            // console.log(liveStore)
            // this.setState({
            //   liveStore: liveStore,
            //   odd:odd

            // });

            // console.log(outputStore)


            // const totalProb = calculateTotalProb(marketById, marketId, outcomeAllIds, -2).totalProb;
            // var market = {}
            // market = sportConfigStore.market;
            // const outcomeOdd = calculateTotalProb(market, outcomeAllIds, -2).outcomeOdd;


            // console.log(this.state.inputStore)
            // var liveStore=[];
            // liveStore=[matchById,oddsFeed]
            // this.setState({
            // liveStore:liveStore
            // });
            // console.log(this.state.liveStore)

          });
      });

    function getSportCollection(sportId, temp ) {
      var tempStoreLive = {
        sportSelectedAllIds: [],
        marketGroups: []
      };
      console.log(temp)
      if (temp[0]["sc_sl"] && temp[0]["sc_sl"] !== 'failed') {
        const sportListLive = buildSportList(temp[0]["sc_sl"], 1);
        tempStoreLive = {
          ...tempStoreLive,
          ...sportListLive
        }
      }
      if (temp[1]["sc_el1"] && temp[1]["sc_el1"] !== 'failed') {
        if (tempStoreLive.sportById[sportId]) {
          const sportSelectedLive = buildSportSelected(1, temp[1]["sc_el1"], 1);
          tempStoreLive = {
            ...tempStoreLive,
            ...sportSelectedLive
          }
        }
      }
      if (temp[2]["sc_gml1"] && temp[2]["sc_gml1"] !== 'failed') {
        const marketGroupsLive = buildMarketGroups(sportId, temp[2]['sc_gml1']);
        tempStoreLive = {
          ...tempStoreLive,
          ...marketGroupsLive
        }
      }
      if (temp[3]["cml1"] && temp[3]["cml1"] !== 'failed') {
        const markets = buildMarkets(sportId, temp[3]["cml1"]);
        tempStoreLive = {
          ...tempStoreLive,
          ...markets
        }
      }
      // if (temp[4]["ctml1_en"] && temp[4]["ctml1_en"] !== 'failed') {
      //   const marketTranslation = buildMarketsTranslation(temp[4]["ctml1_en"],);
      //   tempStoreLive = {
      //     ...tempStoreLive,
      //     ...marketTranslation
      //   }
      // }

      console.log(tempStoreLive)
      return {
        tempStoreLive: tempStoreLive
      }
    }
    function buildSportSelected(sportId, data, live) {
      var sportSelectedAllIds = [];
      var sportSelectedCategAllIds = {};
      var categAllIds = [];
      var categById = {};
      var tourById = {};
      var matchById = {};
      var availableDates = [];
      var matchStore = {};
      var clonesStore = {};
      var configGroup = [];
      var configLive = [];
      var configTour = [];
      var configCateg = [];
      var liveStore = {};
      for (var categId in data) {
        if (data.hasOwnProperty(categId)) {
          var cat = data[categId];
          categAllIds.push(categId);
          var tourAllIds = [];
          for (var tourId in cat[3]) {
            if (cat[3].hasOwnProperty(tourId)) {
              var tour = cat[3][tourId];
              tourAllIds.push(tourId);
              var matchAllIds = [];
              var dateAllIds = [];
              var dateById = {};
              for (var matchId in tour[3]) {
                if (tour[3].hasOwnProperty(matchId)) {
                  var match = tour[3][matchId];
                  matchId = ('l') + matchId;
                  if (match[4] && match[4]['parent_id']) {
                    const parentId = (live ? 'l' : 'p') + match[4]['parent_id'];
                    clonesStore[matchId] = parentId;
                    matchStore[parentId] = {
                      ...matchStore[parentId],
                      cloneId: matchId
                    }
                    matchStore[matchId] = {
                      ...matchStore[matchId],
                      home: match[1],
                      away: match[2],
                      code: match[3],
                      extra: match[4],
                      sportId: sportId * 1,
                      categId: categId * 1,
                      tourId: tourId * 1,
                      groupId: tour[0],
                      live: live
                    }
                    continue;
                  }
                  matchById[matchId] = {
                    ...matchById[matchId],
                    timestamp: match[0],
                    // date: moment(match[0] * 1000).format('L'),
                    // time: moment(match[0] * 1000).format('HH:mm'),
                    home: match[1],
                    away: match[2],
                    code: match[3]
                  }

                  if (match[4]) {
                    matchById[matchId]['extra'] = match[4];
                  }
                  if (live) {
                    matchAllIds.push(matchId);

                  } else {
                    if (!dateById[matchById[matchId].date]) {
                      dateById[matchById[matchId].date] = [];
                      dateAllIds.push(matchById[matchId].date);
                    }
                    dateById[matchById[matchId].date].push(matchId);
                    if (availableDates.indexOf(matchById[matchId].date) == -1) {
                      availableDates.push(matchById[matchId].date);
                    }
                  }
                  matchStore[matchId] = {
                    ...matchStore[matchId],
                    home: match[1],
                    away: match[2],
                    code: match[3],
                    extra: match[4],
                    sportId: sportId * 1,
                    categId: categId * 1,
                    tourId: tourId * 1,
                    groupId: tour[0],
                    live: live
                  }
                  var cc = 'cc1_' + matchStore[matchId]['categId'];
                  if (configCateg.indexOf(cc) === -1) {
                    configCateg.push(cc);
                  }
                  var ct = 'ct1_' + matchStore[matchId]['tourId'];
                  if (configTour.indexOf(ct) === -1) {
                    configTour.push(ct);


                  }
                  var cl = 'cl1_' + [matchId];
                  if (configLive.indexOf(cl) === -1) {
                    configLive.push(cl);
                  }
                  var cg = 'cg1_' + matchStore[matchId]['groupId'];
                  if (configGroup.indexOf(cg) === -1) {
                    configGroup.push(cg);
                  }

                }
                // var matchIds=[];
                // for(matchId in matchById){
                //   matchIds.push(matchById[matchId])
                // }
                // console.log(matchIds)

              }


              if (live) {
                tourById[tourId] = {
                  position: tour[1],
                  name: tour[2],
                  matchAllIds: matchAllIds
                };
              } else {
                tourById[tourId] = {
                  name: tour[2],
                  position: tour[1],
                  dateAllIds: dateAllIds,
                  dateById: dateById
                };
              }
              if (tour[0] !== null) {
                tourById[tourId]['groupId'] = tour[0];
              }
              // liveStore = {
              //   categAllIds: categAllIds,
              //   categById: categById,
              //   tourById: tourById,
              //   matchById: matchById,
              //   marketById: buildMarkets(1, market),
              // }
            }

          }

          const sortedTourIds = tourAllIds.sort((a, b) => tourById[a].position - tourById[b].position);
          categById[categId] = {
            name: cat[1],
            flag: cat[2],
            position: cat[0],
            tourAllIds: sortedTourIds
          };

        }
      }

      const sortedCategIds = categAllIds.sort((a, b) => categById[a].position - categById[b].position);
      sportSelectedAllIds.push(sportId);
      sportSelectedCategAllIds[sportId] = sortedCategIds;

      if (live) {
        return {
          sportSelectedAllIds: sportSelectedAllIds,
          sportSelectedCategAllIds: sportSelectedCategAllIds,
          matchAllIds: matchAllIds,
          tourById: tourById,
          matchById: matchById,
          categById: categById,
          categAllIds: categAllIds,
          tourById: tourById,
          matchById: matchById,

          // matchId: matchId,
          // configCateg: configCateg,
          // configTour: configTour,
          // configGroup: configGroup,
          // configLive: configLive,
          // categId: categId,
          // tourId: tourId,
          // clonesStore: clonesStore,
          // matchStore: matchStore,
          // groupId: tour[0]
        };
      }

      //  else {
      //   return {
      //     sportSelectedAllIds: sportSelectedAllIds,
      //     sportSelectedCategAllIds: sportSelectedCategAllIds,
      //     categById: categById,
      //     tourById: tourById,
      //     matchById: matchById,
      //     availableDates: availableDates.sort()
      //   };
      // }
    }
    function buildSportList(data) {
      var sportById = {};
      var sportAllIds = [];
      for (var sportId in data) {
        if (data.hasOwnProperty(sportId)) {
          var sport = data[sportId];
          sportAllIds.push(sportId);
          sportById[sportId] = {
            name: sport[1],
            position: sport[0],
            nr_match: sport[2]
          }
        }
      }
      const sortedSportIds = sportAllIds.sort((a, b) => sportById[a].position - sportById[b].position);
      return {
        sportById: sportById,
        sportAllIds: sortedSportIds
      }
    }


    function buildMarkets(sportId, data) {
      var marketAllIds = {};
      var marketById = {};
      var mAllIds = [];
      var marketStore = {};
      for (var marketId in data) {
        if (data.hasOwnProperty(marketId)) {
          var market = data[marketId];
          var outcomeAllIds = [];
          var outcomeById = {};
          for (var outcomeId in market[2]) {
            if (market[2].hasOwnProperty(outcomeId)) {
              var outcome = market[2][outcomeId];
              outcomeById[outcomeId] = {
                position: outcome[0],
                name: outcome[1],
                code: outcome[2]
              }

              outcomeAllIds.push(outcomeId);
            }

          }

          const sortedOutcomeAllIds = outcomeAllIds.sort((a, b) => outcomeById[a].position - outcomeById[b].position);
          marketById[marketId] = {
            position: market[0],
            name: market[1],
            outcomeById: outcomeById,
            outcomeAllIds: sortedOutcomeAllIds,
            hasSbv: !!market[3],
            parlay: (market[4] && market[4] === 'corner') ? true : false
          };
          mAllIds.push(marketId);
        }
      }
      marketStore = { ...marketStore, ...marketById };
      const sortedMarketAllIds = mAllIds.sort((a, b) => marketById[a].position - marketById[b].position);
      marketAllIds[sportId] = sortedMarketAllIds;
      return {
        marketById:marketById,
        marketAllIds: marketAllIds,
        marketStore:marketStore
      };
    }
    function buildMarketGroups(sportId, data) {
      var marketGroups = [];
      for (var mGroupId in data) {
        if (data.hasOwnProperty(mGroupId)) {
          marketGroups.push({
            id: mGroupId,
            name: data[mGroupId][0],
            markets: data[mGroupId][1]
          });
        }
      }
      return {
        marketGroups: marketGroups,
      };
    }
    function buildMarketsTranslation(data, marketStore) {
      var marketTranslation = {};
      for (var marketId in data) {
        if (data.hasOwnProperty(marketId)) {
          var market = data[marketId];
          marketTranslation[marketId] = {
            shortName: market[0],
            longName: market[1],
            description: market[2]
          };
          marketStore[marketId] = {
            ...marketStore[marketId],
            ...marketTranslation[marketId]
          }
        }
      }
      return {
        marketTranslation
      };
    }

    function getOdds(temp, live) {
      var oddsFeed = {};
      var extraoddsFeed = {};
      var sportConfigStore = {
        cs: {},
        cc: {},
        cg: {},
        ct: {},
        cl: {}
      }
      for (var key in temp) {
        for (var eventId in temp[key]) {
          if (eventId.startsWith('l')) {
            oddsFeed[eventId] = temp[key][eventId];
            extraoddsFeed = temp[key][eventId];
          }

          if (eventId.startsWith('cs')) {
            sportConfigStore['cs'][eventId] = temp[key][eventId];
          }

          else if (eventId.startsWith('cc')) {
            sportConfigStore['cc'][eventId] = temp[key][eventId];
          }
          else if (eventId.startsWith('cg')) {
            sportConfigStore['cg'][eventId] = temp[key][eventId];
          }
          else if (eventId.startsWith('ct')) {
            sportConfigStore['ct'][eventId] = temp[key][eventId];
          }
          else if (eventId.startsWith('cl')) {
            sportConfigStore['cl'][eventId] = temp[key][eventId];
          }
          sportConfigStore = sportConfigStore;
        }


      }

      return {
        oddsFeed: oddsFeed,
        extraoddsFeed: extraoddsFeed,
        sportConfigStore: sportConfigStore,


      }
    }
    function calculateMarketConfig(sportConfigStore, sportId, categId, tourId, groupId, marketId, live) {
      var marketConfig = {};
      if (live) {
        //  for (marketId in sportConfigStore['cs']['cs1_' + sportId]) {
        if (sportConfigStore['cs']['cs1_' + sportId] && sportConfigStore['cs']['cs1_' + sportId][-2]) {
          marketConfig = { ...marketConfig, ...sportConfigStore['cs']['cs1_' + sportId][-2] };
        }

        // if (sportConfigStore['cs']['cs1_' + sportId] && sportConfigStore['cs']['cs1_' + sportId][marketId]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cs']['cs1_' + sportId][marketId] };
        // }
        // }
        // if (sportConfigStore['cc']['cc1_' + categId] && sportConfigStore['cc']['cc1_' + categId][-2]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cc']['cc1_' + categId][-2] };
        // }
        // if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][marketId] };
        // }

        // if (sportConfigStore['cg']['cg1_' + groupId] && sportConfigStore['cg']['cg1_' + groupId][-2]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cg']['cg1_' + groupId][-2] };
        // }
        // if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][marketId]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][marketId] };
        // }

        // if (sportConfigStore['ct']['ct1_' + tourId] && sportConfigStore['ct']['ct1_' + tourId][-2]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['ct']['ct1_' + tourId][-2] };
        // }
        // if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][marketId]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][marketId] };
        // }

        // if (sportConfigStore['cl'][matchId] && sportConfigStore['cl'][matchId][-2]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cl'][matchId][-2] };
        // }
        // if (sportConfigStore['cl'][matchId] && sportConfigStore['cl'][matchId][marketId]) {
        //   marketConfig = { ...marketConfig, ...sportConfigStore['cl'][matchId][marketId] };
        // }

      }
      return marketConfig;
      //  else {
      //   if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][-1]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][-1] };
      //   }
      //   if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][marketId]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][marketId] };
      //   }

      //   if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][-1]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][-1] };
      //   }
      //   if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][marketId] };
      //   }

      //   if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][-1]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][-1] };
      //   }
      //   if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][marketId]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][marketId] };
      //   }

      //   if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][-1]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][-1] };
      //   }
      //   if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][marketId]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][marketId] };
      //   }

      //   if (sportConfigStore['cp'][matchId] && sportConfigStore['cp'][matchId][-1]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cp'][matchId][-1] };
      //   }
      //   if (sportConfigStore['cp'][matchId] && sportConfigStore['cp'][matchId][marketId]) {
      //     marketConfig = { ...marketConfig, ...sportConfigStore['cp'][matchId][marketId] };
      //   }
      // }  

    }

    // function calculateMarketConfig(sportId, categId, marketId, tourId, groupId, sportConfigStore, live) {
    //   var marketConfig = {}


    //   for (sportId in sportConfigStore['cs']) {
    //     sportConfigStore['cs'][sportId] = sportConfigStore['cs'][sportId]
    //     for (marketId in sportConfigStore['cs'][sportId]) {
    //       sportConfigStore['cs'][sportId][marketId] = sportConfigStore['cs'][sportId][marketId]
    //     }
    //   }
    //   // console.log( sportConfigStore['cs'][sportId][marketId])
    //   console.log(tourId)

    //   for (categId in sportConfigStore['cc']) {
    //     sportConfigStore['cc'][categId] = sportConfigStore['cc'][categId];
    //     for (marketId in sportConfigStore['cc'][categId]) {
    //       sportConfigStore['cc'][categId][marketId] = sportConfigStore['cc'][categId][marketId]
    //     }
    //   }


    //   for (tourId in sportConfigStore['ct']) {
    //     sportConfigStore['ct'][tourId] = sportConfigStore['ct'][tourId]
    //     for (marketId in sportConfigStore['ct'][tourId]) {
    //       sportConfigStore['ct'][tourId][marketId] = sportConfigStore['ct'][tourId][marketId]
    //     }
    //   }

    //   for (groupId in sportConfigStore['cg']) {
    //     sportConfigStore['cg'][groupId] = sportConfigStore['cg'][groupId]
    //     for (marketId in sportConfigStore['cg'][groupId]) {
    //       sportConfigStore['cg'][groupId][marketId] = sportConfigStore['cg'][groupId][marketId]
    //     }
    //   }
    //   // for (matchId in sportConfigStore['cl']) {
    //   //   sportConfigStore['cl'][matchId] = sportConfigStore['cl'][matchId]
    //   //   for(marketId in sportConfigStore['cl'][matchId]){
    //   //     sportConfigStore['cl'][matchId][marketId]=sportConfigStore['cl'][matchId][marketId]
    //   //   }
    //   // }


    //   if (live) {

    //     if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][-2]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][-2] };
    //     }
    //     // console.log(sportConfigStore['cs'][sportId])
    //     // console.log(sportConfigStore['cs'][sportId][-2])
    //     // console.log(sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][-2])

    //     if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][marketId]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][marketId] }

    //     }
    //     // console.log(marketId)
    //     // console.log(sportConfigStore['cs'][sportId])
    //     // console.log(sportConfigStore['cs'][sportId][marketId])
    //     // console.log(sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][marketId])

    //     if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][-2]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][-2] };
    //     }
    //     if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][marketId] };
    //     }
    //     // console.log(sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId])
    //     if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][-2]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][-2] };
    //     }
    //     if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][marketId]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][marketId] };

    //     }

    //     if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][-2]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][-2] };
    //     }
    //     if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][marketId]) {
    //       marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][marketId] };
    //     }
    //     // if (sportConfigStore['cl'][matchId] && sportConfigStore['cl'][matchId][-2]) {
    //     //   marketConfig = { ...marketConfig, ...sportConfigStore['cl'][matchId][-2] };
    //     // }
    //     // if (sportConfigStore['cl'][matchId] && sportConfigStore['cl'][matchId][marketId]) {
    //     //   marketConfig = { ...marketConfig, ...sportConfigStore['cl'][matchId][marketId] };
    //   }

    //   return {
    //     marketConfig: marketConfig
    //   }
    // }
    // function calculateMarketConfig(sportId, sportConfigStore, categId, tourId, groupId, matchId, marketId, live) {
    //   var marketConfig = {};
    //   if (live) {
    //     for (sportId in sportConfigStore['cs']) {
    //       if (sportConfigStore['cs'].hasOwnProperty(sportId)) {
    //         for (marketId in sportConfigStore['cs'][sportId]) {
    //           if (sportConfigStore['cs'][sportId].hasOwnProperty(marketId)) {
    //             if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][-2]) {
    //               marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][-2] };
    //             }
    //             if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][marketId]) {
    //               marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][marketId] };
    //             }
    //           }
    //         }
    //       }
    //     }
    //     for (categId in sportConfigStore['cc']) {
    //       if (sportConfigStore['cc'].hasOwnProperty(categId)) {
    //         for (marketId in sportConfigStore['cc'][categId]) {
    //           if (sportConfigStore['cc'][categId].hasOwnProperty(marketId)) {
    //             if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][-2]) {
    //               marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][-2] };
    //             }
    //             if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId]) {
    //               marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][marketId] };
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    //   return { marketConfig: marketConfig }
    //   for(groupId in sportConfigStore['cg']){
    //     if(sportConfigStore['cg'].hasOwnProperty(groupId)){           

    // if (sportConfigStore['cg1'][groupId] && sportConfigStore['cg1'][groupId][-2]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cg1'][groupId][-2] };
    // }
    // console.log(sportConfigStore['cg1'][groupId])
    // if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][marketId] };
    // }

    //  }
    //   }

    // if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][-2]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][-2] };
    // }
    // if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][marketId] };
    // }

    // if (sportConfigStore['cl'][matchId] && sportConfigStore['cl'][matchId][-2]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cl'][matchId][-2] };
    // }
    // if (sportConfigStore['cl'][matchId] && sportConfigStore['cl'][matchId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cl'][matchId][marketId] };
    // }
    // } else {
    //   if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][-1]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][-1] };
    //   }
    //   if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][marketId]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][marketId] };
    //   }

    //   if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][-1]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][-1] };
    //   }
    //   if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][marketId] };
    //   }

    //   if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][-1]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][-1] };
    //   }
    //   if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][marketId]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][marketId] };
    //   }

    //   if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][-1]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][-1] };
    //   }
    //   if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][marketId]) {
    //     marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][marketId] };
    //   }

    // if (sportConfigStore['cp'][matchId] && sportConfigStore['cp'][matchId][-1]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cp'][matchId][-1] };
    // }
    // if (sportConfigStore['cp'][matchId] && sportConfigStore['cp'][matchId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cp'][matchId][marketId] };
    // }
    // }

    // }
    // else {
    // if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][-1]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][-1] };
    // }
    // if (sportConfigStore['cs'][sportId] && sportConfigStore['cs'][sportId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cs'][sportId][marketId] };
    // }
    // console.log(marketConfig)

    // if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][-1]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][-1] };
    // }
    // if (sportConfigStore['cc'][categId] && sportConfigStore['cc'][categId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cc'][categId][marketId] };
    // }

    // if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][-1]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][-1] };
    // }
    // if (sportConfigStore['cg'][groupId] && sportConfigStore['cg'][groupId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cg'][groupId][marketId] };
    // }

    // if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][-1]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][-1] };
    // }
    // if (sportConfigStore['ct'][tourId] && sportConfigStore['ct'][tourId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['ct'][tourId][marketId] };
    // }

    // if (sportConfigStore['cp'][matchId] && sportConfigStore['cp'][matchId][-1]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cp'][matchId][-1] };
    // }
    // if (sportConfigStore['cp'][matchId] && sportConfigStore['cp'][matchId][marketId]) {
    //   marketConfig = { ...marketConfig, ...sportConfigStore['cp'][matchId][marketId] };
    // }
    // }
    function calculateTotalProb(market, oddsFeed, marketById, marketId, outcomeById, matchById, matchId, live, marketOdds) {
      for (var key in oddsFeed) {
        for (marketId in oddsFeed[key]) {
          for (var temp in oddsFeed[key][marketId]) {

            marketOdds = oddsFeed[key][marketId][temp];

            var totalProb = 0;
            if (marketById[marketId] != undefined) {

              marketById[marketId].outcomeAllIds.forEach(outcomeId => {
                var outcomeOdd = 100;
                if (marketOdds != undefined) {
                  if (marketOdds && marketOdds[outcomeId]) {
                    if (live) {
                      outcomeOdd = marketOdds[outcomeId][0];

                    }
                    else {

                      outcomeOdd = marketOdds[outcomeId].position;
                    }
                  }
                }
                totalProb += 1 / outcomeOdd;

              });





            }
            return { totalProb: totalProb };
          }
        }

      }




    }

    // function buildMatchOdds(sportConfigStore, marketStore, outcomeAllIds, matchId, marketId, matchStore, inputStore, outputStore, oddsChangeStore) {
    //   const { sportId, categId, tourId, groupId, live } = matchStore[matchId];
    //   for (marketId in inputStore) {
    //     if (inputStore.hasOwnProperty(marketId)) {
    //       if (inputStore[marketId] !== null) {
    //         if (marketStore[marketId] && marketStore[marketId].outcomeAllIds) {
    //           var marketConfig = calculateMarketConfig(sportConfigStore, sportId, categId, tourId, groupId, marketId, live);
    //           if (marketConfig.s != 1) {
    //             for (var sbv in inputStore[marketId]) {
    //               if (inputStore[marketId].hasOwnProperty(sbv) && inputStore[marketId][sbv]) {
    //                 if (sbv == 'cloneId') {
    //                   continue;
    //                 }
    //                 var totalProb = undefined;
    //                 var balance;
    //                 if (marketConfig.po && marketConfig.po > 0 && marketConfig.po != 100) {
    //                   totalProb = calculateTotalProb(marketStore[marketId], inputStore[marketId][sbv], live);
    //                   if (1 / marketConfig.po > totalProb) {
    //                     if (marketConfig.balance) {
    //                       balance = marketConfig.balance;

    //                     } else {
    //                       balance = 1 / marketStore[marketId].outcomeAllIds.length;
    //                     }

    //                   } else {
    //                     totalProb = undefined;
    //                   }

    //                 }
    //                 marketStore[marketId].outcomeAllIds.forEach(outcomeId => {
    //                   var outcomeOdd = undefined;
    //                   var outcomeOddObj = {};
    //                   if (live) {
    //                     if (inputStore[marketId][sbv][outcomeId] && inputStore[marketId][sbv][outcomeId][0] !== 0 && inputStore[marketId][sbv][outcomeId][0] !== null) {
    //                       outcomeOdd = inputStore[marketId][sbv][outcomeId][0];
    //                       outcomeOdd = calculateOdd(outcomeOdd, marketConfig, totalProb, balance, outcomeId);
    //                       outcomeOddObj = {
    //                         odd: outcomeOdd,
    //                         status: inputStore[marketId][sbv][outcomeId][1]
    //                       }
    //                     }


    //                   } else if (inputStore[marketId][sbv][outcomeId] !== 0 && inputStore[marketId][sbv][outcomeId] !== null) {
    //                     outcomeOdd = inputStore[marketId][sbv][outcomeId];
    //                     outcomeOdd = calculateOdd(outcomeOdd, marketConfig, totalProb, balance, outcomeId);
    //                     outcomeOddObj = {
    //                       odd: outcomeOdd
    //                     }


    //                   }
    //                   // console.log(outcomeOddObj)

    //                   if (outcomeOdd) {

    //                     if (!outputStore[marketId]) {
    //                       outputStore[marketId] = {};

    //                     }
    //                     if (!outputStore[marketId][sbv]) {
    //                       outputStore[marketId][sbv] = {};
    //                     }
    //                     // TODO: change store update
    //                     if (live && outputStore[marketId][sbv][outcomeId] && outputStore[marketId][sbv][outcomeId].odd) {
    //                       if (outcomeOdd - outputStore[marketId][sbv][outcomeId].odd > 0) {
    //                         oddsChangeStore['mid' + matchId + '_m' + marketId + '_' + sbv + '_o' + outcomeId] = Date.now();
    //                       } else if (outcomeOdd - outputStore[marketId][sbv][outcomeId].odd < 0) {
    //                         oddsChangeStore['mid' + matchId + '_m' + marketId + '_' + sbv + '_o' + outcomeId] = -1 * Date.now();
    //                       } else {
    //                         if (Date.now() - oddsChangeStore['mid' + matchId + '_m' + marketId + '_' + sbv + '_o' + outcomeId] > 10000) {
    //                           delete oddsChangeStore['mid' + matchId + '_m' + marketId + '_' + sbv + '_o' + outcomeId];
    //                         }
    //                       }
    //                     }
    //                     outputStore[marketId][sbv][outcomeId] = outcomeOddObj;
    //                   }
    //                 });
    //                 // if (hasOdds) {
    //                 //   buildOddsCalculatedStore(marketConfig, matchId, marketId, sbv, live);
    //                 // } else {
    //                 //   delete inputStore[marketId][sbv];
    //                 //   if (outputStore && outputStore[marketId]) {
    //                 //     delete outputStore[marketId][sbv];
    //                 //   }
    //                 // }
    //               }
    //             }
    //             if (Object.keys(inputStore[marketId]).length == 0) {
    //               delete inputStore[marketId];
    //               if (outputStore) {
    //                 delete outputStore[marketId];
    //               }
    //             }
    //             else {
    //               for (var sbv_o in outputStore[marketId]) {
    //                 if (!inputStore[marketId][sbv_o]) {
    //                   delete outputStore[marketId][sbv_o];
    //                 }
    //               }
    //             }
    //             if (outputStore[marketId] && inputStore[marketId]['cloneId']) {
    //               outputStore[marketId]['cloneId'] = inputStore[marketId]['cloneId'];
    //             }
    //             // if (marketStore[marketId].hasSbv && outputStore[marketId]) {
    //             //   var mbbSbv = getMbSbv(marketStore[marketId], outputStore[marketId]);
    //             //   if (mbbSbv) {
    //             //     outputStore[marketId].mbbSbv = mbbSbv;
    //             //   }
    //             // }
    //           } else {
    //             delete inputStore[marketId];
    //             if (outputStore) {
    //               delete outputStore[marketId];
    //             }
    //           }
    //         } else if (isNaN(marketId * 1)) {
    //           if (!outputStore) {
    //             outputStore = {}
    //           }
    //           outputStore[marketId] = inputStore[marketId]
    //         }
    //       } else {
    //         delete inputStore[marketId];
    //         if (outputStore) {
    //           delete outputStore[marketId];
    //         }
    //       }
    //     }
    //   }
    //   console.log(outputStore)
    //   return { outputStore: outputStore }

    // }
    // function getMbSbv(marketById, marketId, market, outcomeAllIds, marketOdds) {


    //   var mbbSbv = undefined;
    //   var minDeviation = 99999999
    //   for (var key in marketById) {
    //     marketOdds = marketById[key].outcomeById;
    //     if (Object.keys(marketOdds).length == 1) {
    //       return Object.keys(marketOdds)[0];
    //     }

    //     for (var sbv in marketOdds) {
    //       if (sbv !== 'mbbSbv') {
    //         var outcomeArr = [];

    //         market.forEach(outcomeId => {
    //           var outcomeOdd;
    //           if (marketOdds && marketOdds[sbv][outcomeId]) {
    //             outcomeOdd = marketOdds[sbv][outcomeId].odd;
    //             outcomeArr.push(outcomeOdd);
    //             console.log(outcomeArr)
    //           }

    //         });
    //         if (outcomeArr.length > 2) {
    //           var sum = 0;
    //           outcomeArr.forEach(odd => sum += odd);
    //           var mean = sum / outcomeArr.length;
    //           var dev = 0;
    //           outcomeArr.forEach(odd => {
    //             dev += Math.abs(odd - mean);
    //           });
    //           if (dev < minDeviation) {
    //             minDeviation = dev;
    //             mbbSbv = sbv;
    //           }
    //         } else if (outcomeArr.length == 2) {
    //           var dev = Math.abs(outcomeArr[1] - outcomeArr[0]);
    //           if (dev < minDeviation) {
    //             minDeviation = dev;
    //             mbbSbv = sbv;
    //           }
    //         } else {
    //           if (outcomeArr[0] < minDeviation) {
    //             minDeviation = outcomeArr[0];
    //             mbbSbv = sbv;
    //           }
    //         }
    //       }
    //     }
    //     return { mbbSbv: mbbSbv };
    //   }
    // }
    function calculateOdd(outcomeOdd, marketConfig, totalProb, balance, outcomeId) {
      if (totalProb) {
        outcomeOdd = (1 / ((1 / outcomeOdd) + ((1 / marketConfig.po) - totalProb) * balance));
      }
      // if (marketConfig['cf_' + outcomeId] && marketConfig['cf_' + outcomeId] > 0 && marketConfig['cf_' + outcomeId] < 2) {
      //   outcomeOdd *= marketConfig['cf_' + outcomeId];
      // }
      // var min = 100;
      // if (marketConfig.min) {
      //   min = marketConfig.min * 100;
      // }
      // if (outcomeOdd < min) {
      //   outcomeOdd = undefined;
      // }
      // if (marketConfig.max && outcomeOdd > marketConfig.max * 100) {
      //   outcomeOdd = marketConfig.max * 100;
      // }
      return Math.round(outcomeOdd)
    }

    // function getMbSbv(market,marketById, marketId,marketOdds) {
    //   for (var key in marketById) {
    //     marketOdds = marketById[key].outcomeById;
    //   }

    //   var mbbSbv = undefined;
    //   var minDeviation = 99999999;  
    //   if (Object.keys(marketOdds).length == 1) {
    //     return Object.keys(marketOdds)[0];
    //   }
    //   console.log(Object.keys(marketOdds)[0])
    //   for (var sbv in marketOdds) {
    //     if (sbv !== 'mbbSbv') {
    //       var outcomeArr = [];
    //       marketById[marketId].outcomeAllIds.forEach(outcomeId => {
    //         var outcomeOdd;
    //         if (marketOdds && marketOdds[sbv][outcomeId]) {
    //           outcomeOdd = marketOdds[sbv][outcomeId].odd;

    //         }
    //         outcomeArr.push(outcomeOdd);
    //       });
    //       if (outcomeArr.length > 2) {
    //         var sum = 0;
    //         outcomeArr.forEach(odd => sum += odd);
    //         var mean = sum / outcomeArr.length;
    //         var dev = 0;
    //         outcomeArr.forEach(odd => {
    //           dev += Math.abs(odd - mean);
    //         });
    //         if (dev < minDeviation) {
    //           minDeviation = dev;
    //           mbbSbv = sbv;
    //         }
    //       } else if (outcomeArr.length == 2) {
    //         var dev = Math.abs(outcomeArr[1] - outcomeArr[0]);
    //         if (dev < minDeviation) {
    //           minDeviation = dev;
    //           mbbSbv = sbv;
    //         }
    //       } else {
    //         if (outcomeArr[0] < minDeviation) {
    //           minDeviation = outcomeArr[0];
    //           mbbSbv = sbv;
    //         }
    //       }
    //     }
    //   }
    //   return mbbSbv;
    // }

  }

  render() {
 

 

    return (
      <div>
        <div>
          <Header market={this.state.market}></Header>
        </div>
        <div>
          {/* {console.log(this.state.tempStoreLive.sportSelectedAllIds)} */}
          <Result tempStoreLive={this.state.tempStoreLive}></Result>
          {/* <Out dataFromParent = {outputStore}/> */}
          {/* <Out data={liveStore}
              odd ={this.state.odd}
          ></Out> */}
          {/* {this.state.tempStoreLive.liveStore.matchById.map((matchId, i) => <Out matchId={matchId} key={i} />)} */}

          {/* {console.log(outputStore)}</div>
          <div>
            // <Redc redc={outputStore[5]}></Redc> */}
          {/* {inputStore.map((score, i) => <Out score={score} key={i} />)} */}
          {/* {outputStore.map((score, i) => <Out score={score} key={i} />)} */}

        </div>
        {/* <Out>{this.state.inputStore}</Out> */}
        {/* {console.log(this.state.inputStore)} */}
        {/* {Object.matchIds.map(item => {
            const away = {}
            const home ={}
            // return <AW >{container[item.away] = item.away}</AW>
            //         <HM>{container[item.home] = item.home}</HM>
            away[item.away]=item.away;
            home[item.home]=item.home;
          <AW > {away[item.away]=item.away}</AW>
            // away=away[{item.away];
            // container[}item.home]=item.home;
                  return {away,
                  home};
                
          } )
  
        // <AW>{away.container[]}</AW>
          } */}

        {/* {Object.keys(matchIds).map(key =>
      <AW value={key}>{matchIds[key].home}</AW>
    )} */}
        {/* {   matchIds.map(item => {
 

    <AW>{ item.home}</AW>;
    

}


)
} */}



      </div>
      // {/* <div>
      //   {console.log(this.state.away)}
      //   {/* <AW away={this.state.away.away}></AW> */}



    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root'));